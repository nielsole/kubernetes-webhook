package main

import (
	yaml "gopkg.in/yaml.v2"
	"io/ioutil"
	"path/filepath"
)

type SettingsCredential struct {
	Key        string   `yaml:"key"`
	UserGlobs  []string `yaml:"userglobs"`
	GroupGlobs []string `yaml:"groupglobs"`
}

type Settings struct {
	Credentials   *[]SettingsCredential `yaml:"credentials"`
	ListenAddress string                `yaml:"listenaddress"`
	SSL           bool                  `json:"ssl"`
	SSLKeyPath    string                `json:"sslkeypath"`
	SSLCrtPath    string                `json:"sslcrtpath"`
}

func readConfig(path string) (error, *Settings) {
	realPath, err := filepath.EvalSymlinks(path)
	if err != nil {
		return err, nil
	}
	data, err := ioutil.ReadFile(realPath)
	if err != nil {
		return err, nil
	}

	settings := Settings{}
	err = yaml.Unmarshal(data, &settings)
	if err != nil {
		return err, nil
	}

	return nil, &settings
}
